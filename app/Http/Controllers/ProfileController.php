<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Registrar;
use Auth;

class ProfileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Update Profiles Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling profile update requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	/**
	 * The registrar implementation.
	 *
	 * @var \Illuminate\Contracts\Auth\Registrar
	 */
	protected $registrar;

	/**
	 * Create a new profile controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Regsitrar  $registrar
	 * @return void
	 */
	public function __construct(Registrar $registrar)
	{
		$this->registrar = $registrar;

		$this->middleware('auth');
	}
	
	/**
	 * Show the profile update form.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function getUpdate( $id = null )
	{
		// If the id is not set OR the id IS set and the user is NOT an administrator, show logged in user.
		if ( ! isset( $id ) || 0 == Auth::user()['administrator'] ) $id = Auth::id();

		$user = User::where('id',$id)->first();
		return view('profile')->with( ['user' => $user, 'title' => 'Update Profile'] );
	}
	
	/**
	 * Show the user list.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getUserlist()
	{
		$users = User::all();
		return view('userlist')->with( 'users', $users );
	}
	
	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postUpdate(Request $request)
	{
		// If the id is NOT the current user AND the user is NOT an administrator, dump to the welcome screen.
		if ( Auth::id() != $request->__get('id') && 0 == Auth::user()['administrator'] ) return view('welcome');

		$validator = $this->registrar->updateValidator($request->all());
	
		if ($validator->fails())
		{
			$this->throwValidationException(
					$request, $validator
			);
		}
	
		$this->registrar->update($request->all());

		return view('home');
	}

	/**
	 * Show the new profile form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getCreate()
	{
		// If the id is not set OR the id IS set and the user is NOT an administrator, show logged in user.
		if ( ! isset( $id ) || 0 == Auth::user()['administrator'] ) $id = Auth::id();
	
		$user = new User;
		$user->active = 1;
		return view('profile')->with( ['user' => $user, 'title' => 'Create User'] );
	}

	/**
	 * Handle a create user request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postCreate(Request $request)
	{
		// If the user is NOT an administrator, dump to the welcome screen.
		if ( 0 == Auth::user()['administrator'] ) return view('welcome');

		$validator = $this->registrar->updateValidator($request->all());
	
		if ($validator->fails())
		{
			$this->throwValidationException(
					$request, $validator
			);
		}
	
		$this->registrar->createUser($request->all());

		return view('home');
	}

}
