<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use Auth;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'first_name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Get a validator for an incoming profile update request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function updateValidator(array $data)
	{
		return Validator::make($data, [
			'first_name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users,email,'.$data['id'],
			'password' => 'confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return User::create([
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);
	}

	/**
	 * Update a user profile.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function update(array $data)
	{
		$updates = [
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'addr1' => $data['addr1'],
			'addr2' => $data['addr2'],
			'city' => $data['city'],
			'state' => $data['state'],
			'zip' => $data['zip'],
			'email' => $data['email'],
			'password' => bcrypt($data['password'])];

		// if the user is an admin and NOT the user being adjusted, set the "active" column.
		// The users should not be able to inactivate themselves.
		if ( 1 == Auth::user()['administrator'] && Auth::id() != $data['id'] )
			$updates['active'] = isset( $data['active'] ) ? 1 : 0;

		if ( '' == $data['password'] ) unset( $updates['password'] );

		return User::where('id', $data['id'])->update( $updates );
	}

	/**
	 * Create a new user by administrator.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function createUser(array $data)
	{
		$newUser = [
				'first_name' => $data['first_name'],
				'last_name' => $data['last_name'],
				'addr1' => $data['addr1'],
				'addr2' => $data['addr2'],
				'city' => $data['city'],
				'state' => $data['state'],
				'zip' => $data['zip'],
				'email' => $data['email'],
				'active' => isset( $data['active'] ) ? 1 : 0,
				'password' => bcrypt($data['password'])];

		return User::create( $newUser );
	}
	
}
