@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{ $title }}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ $user->id }}">

						<div class="form-group">
							<label class="col-md-4 control-label">First name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Last name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ $user->email }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Address Line 1</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="addr1" value="{{ $user->addr1 }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Address Line 2</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="addr2" value="{{ $user->addr2 }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">City</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="city" value="{{ $user->city }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">State</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="state" value="{{ $user->state }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">ZIP Code</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="zip" value="{{ $user->zip }}">
							</div>
						</div>

						@if ( Auth::user()->administrator )
						<div class="form-group">
							<label class="col-md-4 control-label">Active</label>
							<div class="col-md-1">
								<input type="checkbox" class="form-control" name="active" value="1" @if ( $user->active ) checked="checked" @endif>
							</div>
						</div>
						@endif
					
						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Administrator</label>
							<div class="col-md-6">
								<label class="control-label">@if ( $user->administrator ) Yes @else No @endif</label>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									{{ $title }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
