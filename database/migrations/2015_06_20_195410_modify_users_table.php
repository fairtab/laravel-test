<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
			$table->renameColumn( 'name', 'first_name' );
			$table->string( 'last_name' )->nullable();
			$table->string( 'addr1' )->nullable();
			$table->string( 'addr2' )->nullable();
			$table->string( 'city' )->nullable();
			$table->string( 'state' )->nullable();
			$table->string( 'zip' )->nullable();
			$table->tinyInteger( 'administrator' )->default( 0 );
			$table->tinyInteger( 'active' )->default( 1 );
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($table)
		{
			$table->renameColumn( 'first_name', 'name' );
			$table->dropColumn(	array( 'last_name', 'addr1', 'addr2', 'city', 'state', 'zip', 'administrator', 'active' ) );
		});
	}

}
