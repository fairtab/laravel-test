@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">User List</div>

				@if ( Auth::user()->administrator )
				<div class="panel-body">
					<div class="row">
						<table class="table table-striped">
					    	<thead>
								<tr>
									<th>Email</th>
									<th>Name</th>
									<th>Active?</th>
								</tr>
							</thead>
							<tbody>
							@foreach($users as $user)
							<tr>
								<td><a href="/profile/update/{{ $user->id }}">{{ $user->email }}</a></td>
								<td>{{ $user->first_name . " " . $user->last_name }}</td>
								<td>@if ( 1 == $user->active ) Yes @else No @endif</td>
							</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
				@else
				<div class="panel-body">
					You are not authorized to view this page. 
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
